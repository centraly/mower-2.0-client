
export default class MotorController {

    getLeftMotorSetSpeedCmd(speed) {
      if (speed >= 0) return JSON.stringify({arduino: ['left_wheel_motor_forward_set_speed', speed]})
      return JSON.stringify({arduino: ['left_wheel_motor_backward_set_speed', Math.abs(speed)]})
    }

    getRightMotorSetSpeedCmd(speed) {
      if (speed >= 0) return JSON.stringify({arduino: ['right_wheel_motor_forward_set_speed', speed]})
      return JSON.stringify({arduino: ['right_wheel_motor_backward_set_speed', Math.abs(speed)]})
    }

    getMowerMotorStartCmd () {
      return JSON.stringify({mower_motor_run: []})
    }

    getMowerMotorStopCmd() {
      return JSON.stringify({mower_motor_stop: []})
    }
}

